"""_____________________________________________________________________

:PROJECT: LARA-django Data Sync

* Main module implementation *

:details:  Main module implementation.

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

import asyncio
import environ
from typing import Optional
from lara_django_sync.sync_logger import get_logger

from lara_django_sync.lara_django_sync_interface import LARASyncInterface

class LARA2LARASyncIncr(LARASyncInterface):
    def __init__(self, 
                 source_host: Optional[str] = None,
                 source_port: Optional[int] = None ) -> None:
        """Implementation of the SyncInterface
        """
        self.logger = get_logger(__name__)
        self.logger.debug("LARA2LARASync.__init__")

        self.env = environ.Env(
            # set casting, default value
            DEBUG=(bool, False)
        )
        
        self.source_host = self.env('DJANGO_HOST', default=source_host)
        self.source_port = self.env('DJANGO_PORT', default=source_port)

        self.logger.debug(f"source_host: {self.source_host}")
        self.logger.debug(f"source_port: {self.source_port}")

        # add LARA gRPC client here
        
    async def sync_projects(self) -> None:
        """implementation of an incremental sync method
        """
        self.logger.debug(f"Sync all items of projects incrementally")

        # ask target server for sync projects
        
        # ask for LARA projects

        projects_list = ["proj1", "proj2"]

        await asyncio.gather(*( self.sync_project(project) for project in projects_list) )


    async def sync_project(self, project):

        self.logger.debug(f"------ syncing {project} incr. -------")

        
        # TODO: always check, if project (and all related items) already exists

        await asyncio.gather( 
            #self.sync_projects(),
            self.sync_experiments(),
            self.sync_devices(),
            self.sync_substances(),
        )

    async def sync_experiments(self) -> None:
        """implementation of the sync_experiments method
        """
        self.logger.debug(f"Sync experiments ... ")
        await asyncio.sleep(3)
        self.logger.debug(f"Sync experiments ... done")

    async def sync_devices(self) -> None:
        """implementation of the sync_devices method
        """
        self.logger.debug(f"Syncing devices ... ")
        await asyncio.sleep(2)
        self.logger.debug(f"Syncing devices ... done")
        

    async def sync_substances(self) -> None:
        """implementation of the sync_substances method
        """
        self.logger.debug(f"Sync substances ...")
        await asyncio.sleep(1)
        self.logger.debug(f"Sync substances ... done")

    async def sync_projects_incremental(self) -> None:
        """implementation of the sync_incremental method
        """
        self.logger.debug(f"Sync incremental: ")
        await asyncio.sleep(1)
        self.logger.debug(f"Sync incremental: done")