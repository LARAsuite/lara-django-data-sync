#!/usr/bin/env python
"""Tests for `lara_django_sync` package."""
# pylint: disable=redefined-outer-name
import pytest
from lara_django_sync import __version__
from lara_django_sync.lara_django_sync_interface import LARASyncInterface
from lara_django_sync.lara_django_full_sync_impl import LARA2LARASyncFull

@pytest.mark.skip("Version needs to be updated")
def test_version():
    """Sample pytest test function."""
    assert __version__ == "0.0.1"

def test_SyncInterface():
    """ testing the formal interface (SyncInterface)
    """
    assert issubclass(LARA2LARASyncFull, LARASyncInterface)

def test_LARA2LARASync():
    """ Testing LARA2LARASyncFull class
    """
    l2l = LARA2LARASyncFull()
    assert isinstance(l2l, LARASyncInterface)

