# LARA-django Data Sync

LARA-django Data Sync is a python library for Synchronisation of LARA instances.
LARA-django Data Sync - a Peer2peer Data Synchroniser


LARA-django Data Sync utilises common Peer-to-peer infrastructures, like the bittorrent network to connect two LARA instances across firewalls.


Data Synchronisation only happens with user/scientist selected data of projects and experiments.
Experimental data is recursively synchronised.

Sharing public data with a bittorrent like p2p network should also be considered.


Safety considerations are very important.


## Features

## Installation

    pip install lara-django-data-sync --index-url https://gitlab.com/api/v4/projects/47146485/packages/pypi/simple

## Usage

    # printing the help
    lara-sync --help 

    lara-sync --full # full sync of all selected projects and experiments


## Development

    git clone gitlab.com/LARAsuite/lara-django-data-sync

    # create a virtual environment and activate it then run

    pip install -e .[dev]

    # run unittests

    invoke test   # use the invoke environment to manage development
    

## Documentation

The Documentation can be found here: [https://LARAsuite.gitlab.io/lara-django-data-sync](https://LARAsuite.gitlab.io/lara-django-data-sync) 


## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter)
 and the [gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) project template.



