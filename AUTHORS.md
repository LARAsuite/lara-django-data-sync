
# Acknowledgements and Credits

The LARA-django Data Sync project thanks


Contributors
------------

* Mickey Kim <mickey.kim@genomicsengland.co.uk>  ! Thanks for the phantastic cookiecutter template !


Development Lead
----------------

* mark doerr <mark.doerr@uni-greifswald.de>