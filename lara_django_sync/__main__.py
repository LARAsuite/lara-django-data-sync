#!/usr/bin/env python3
# vim:fileencoding=utf-8
"""_____________________________________________________________________

:PROJECT: LARA-django Data Sync

* Main module command line interface *

:details:  Main module command line interface. 
           !!! Warning: it should have a diffent name than the package name.

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

"""Main module implementation. !!! Warning: it should have a diffent name than the package name. """
"""Console script for lara_django_sync."""


import logging
import argparse
import sys
import asyncio

from lara_django_sync.sync_logger import get_logger
from lara_django_sync import __version__

from lara_django_sync.lara_django_full_sync_impl import LARA2LARASyncFull
from lara_django_sync.lara_django_incr_sync_impl import LARA2LARASyncIncr

logging.basicConfig(level=logging.DEBUG)    

def parse_command_line():
    """ Looking for command line arguments"""
    
    description = "lara_django_sync"
    parser = argparse.ArgumentParser(description=description)

    # parser.add_argument("_", nargs="*")
    parser.add_argument(
        "-f", "--full", action="store_true", help="full sync, not only changes"
    )

    parser.add_argument(
        "-i", "--incremental", action="store_true", help="perform incremental sync = sync only changes"
    )
    parser.add_argument(
        "--source-host", action="store", help="LARA source gRPC server name or IP"
    )
    parser.add_argument(
        "--source-port", action="store", help="LARA source gRPC server port number"
    )

    parser.add_argument('-v', '--version', action='version', version='%(prog)s ' + __version__)

    # add more arguments here

    return parser.parse_args()

async def main():
    """Console script for lara_django_sync."""
        # or use logger.INFO (=20) or logger.ERROR (=30) for less output

    logger = get_logger(__name__) #__file__)
    
    args = parse_command_line()
        
    if len(sys.argv) <= 2:
        logger.debug("no arguments provided !")
        logger.debug("try --help for more information")

    if args.incremental:
        logger.debug("incremental sync ... ")

    if args.full:
        L2L_sync = LARA2LARASyncFull(source_host=args.source_host, source_port=args.source_port)
        await L2L_sync.sync_projects()
    elif args.incremental:
        L2L_sync = LARA2LARASyncIncr(source_host=args.source_host, source_port=args.source_port)
        await L2L_sync.sync_projects()
    
    return 0

def main_async():
    asyncio.run(main())

if __name__ == "__main__":
    sys.exit(asyncio.run(main()))  # pragma: no cover
