"""_____________________________________________________________________

:PROJECT: LARA-django Data Sync

* Main module implementation *

:details:  Main module implementation.

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
import grpc
import asyncio
import environ
from typing import Optional
from lara_django_sync.sync_logger import get_logger

from lara_django_sync.lara_django_sync_interface import LARASyncInterface

import lara_django_base_grpc.v1.lara_django_base_pb2 as lara_django_base_pb2
import lara_django_base_grpc.v1.lara_django_base_pb2_grpc as lara_django_base_pb2_grpc

import lara_django_projects_grpc.v1.lara_django_projects_pb2 as lara_django_proj_pb2
import lara_django_projects_grpc.v1.lara_django_projects_pb2_grpc as lara_django_proj_pb2_grpc


class LARA2LARASyncFull(LARASyncInterface):
    def __init__(self, 
                 source_host: Optional[str] = None,
                 source_port: Optional[int] = None ) -> None:
        """Implementation of the SyncInterface
        """
        self.logger = get_logger(__name__)
        self.logger.debug("LARA2LARASync.__init__")

        self.env = environ.Env(
            # set casting, default value
            DEBUG=(bool, False)
        )
        
        self.source_host = self.env('DJANGO_HOST', default=source_host)
        self.source_port = self.env('DJANGO_PORT', default=source_port)

        lara_channel = grpc.insecure_channel(f'{self.source_host}:{self.source_port}')

        self.namespace_client = lara_django_base_pb2_grpc.NamespaceControllerStub(lara_channel)
        self.laraserv_client = lara_django_base_pb2_grpc.LARAServerControllerStub(lara_channel)
        self.proj_client = lara_django_proj_pb2_grpc.ProjectControllerStub(lara_channel)
        self.proj_sync_client = lara_django_proj_pb2_grpc.ProjectsynchronisationControllerStub(lara_channel)

        self.logger.debug(f"source_host: {self.source_host}")
        self.logger.debug(f"source_port: {self.source_port}")

        # add LARA gRPC client here
        
    async def sync_projects(self) -> None:
        """implementation of the sync_all method
        """
        self.logger.debug(f"Sync all items of projects")

        # ask target server for sync projects
        self.target_servers = self.laraserv_client.List(lara_django_base_pb2.LARAServerListRequest()).results
        
        self.logger.debug(f"target_servers: {self.target_servers}")

        # ask for LARA projects
        sync_projects = self.proj_sync_client.List(lara_django_proj_pb2.ProjectsynchronisationListRequest()).results

        projects_list = [sync_project.projects for sync_project in sync_projects]


        await asyncio.gather(*( self._sync_projects(project) for project in projects_list) )

    async def _sync_projects(self, projects):

        self.logger.debug(f"------ syncing {projects} -------")

        await asyncio.gather(*( self._sync_project(project) for project in projects) )


    async def _sync_project(self, project):

        self.logger.debug(f"------ syncing {project} -------")

        # TODO: always check, if project (and all related items) already exists

        await asyncio.gather( 
            #self.sync_projects(),
            self.sync_experiments(),
            self.sync_devices(),
            self.sync_substances(),
        )

    async def sync_experiments(self) -> None:
        """implementation of the sync_experiments method
        """
        self.logger.debug(f"Sync experiments ... ")
        await asyncio.sleep(3)
        self.logger.debug(f"Sync experiments ... done")

    async def sync_devices(self) -> None:
        """implementation of the sync_devices method
        """
        self.logger.debug(f"Syncing devices ... ")
        await asyncio.sleep(2)
        self.logger.debug(f"Syncing devices ... done")
        

    async def sync_substances(self) -> None:
        """implementation of the sync_substances method
        """
        self.logger.debug(f"Sync substances ...")
        await asyncio.sleep(1)
        self.logger.debug(f"Sync substances ... done")

    async def sync_projects_incremental(self) -> None:
        """implementation of the sync_incremental method
        """
        self.logger.debug(f"Sync incremental: ")
        await asyncio.sleep(1)
        self.logger.debug(f"Sync incremental: done")
        
