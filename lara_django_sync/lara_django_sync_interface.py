"""_____________________________________________________________________

:PROJECT: LARA-django Data Sync

* Main module formal interface. *

:details: In larger projects, formal interfaces are helping to define a trustable contract.
          Currently there are two commonly used approaches: 
          [ABCMetadata](https://docs.python.org/3/library/abc.html) or [Python Protocols](https://peps.python.org/pep-0544/)

       see also:
       ABC metaclass
         - https://realpython.com/python-interface/
         - https://dev.to/meseta/factories-abstract-base-classes-and-python-s-new-protocols-structural-subtyping-20bm

.. note:: -
.. todo:: - 
________________________________________________________________________
"""


# here is a 
import asyncio
from abc import ABCMeta, abstractclassmethod

class LARASyncInterface(metaclass=ABCMeta):
    """ Synchroniser formal Interface
        TODO: test, if ABC baseclass is wor
    """
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'sync_projects_full') and 
                callable(subclass.sync_projects_full) or 
                callable(subclass.sync_projects_incremental) or
                NotImplemented)
        
    @abstractclassmethod 
    async def sync_projects(self) -> None:
        """sync LARA based on projects
        
        #:param name: person to greet
        :type name: None
        """